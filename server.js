var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var router = express.Router();
var appRoutes = require('./app/routes/api')(router);
var awsStackRoutes = require('./app/routes/rehydration')(router);
var ec = require('./app/routes/ec')(router);
var path = require('path');
var passport = require('passport');
var cookieParser = require('cookie-parser');
// var session = require('session');
// var MongoStore = require('connect-mongo')(session);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname+'/public'));
app.use('/api',appRoutes);
app.use('/stack',awsStackRoutes);
app.use('/ec',ec);

// app.set(cookieParser());
// app.use(session({
//     secret: 'sessionKey',
//  }));
mongoose.connect('mongodb://localhost:27017/axon-DB',function(err){
	if (err){
		console.log('Not connected to the database'+ err);
	}else {
		console.log('Successfully connected to MongoDB');
	}
});

app.get('*',function(req,res){
	res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

app.listen(port,function(){
	console.log("Running the server on port "+ port);
});