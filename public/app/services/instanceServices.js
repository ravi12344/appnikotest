angular.module('instanceServices', [])
.factory('Instance', function($http,Config) {
    var instanceFactory = {}; // Create the userFactory object
    var url=Config.url;
    
    instanceFactory.getInstances = function(name, location){
        var tempUrl = url + location + '/view/instances;tags.value=ASV'+name+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    instanceFactory.getInstance = function(instanceId){
        var tempUrl = url + '/view/instances/' + instanceId + ';_pp';
        console.log(tempUrl);
        return $http.get(tempUrl);
    }
    instanceFactory.getImage = function(imageId){
        var tempUrl = url + '/aws/images/' + imageId + ';_pp';
        console.log(tempUrl);0
        return $http.get(tempUrl);
    }
    instanceFactory.action = function(instance){
        var tempUrl = "/ec/actionInstance";
        return $http.post(tempUrl,instance);
    }
    instanceFactory.setCredentials = function(rolename){
        var tempUrl = "/ec/setCredentials";
        return $http.post(tempUrl,rolename);
    }
    return instanceFactory; // Return instanceFactory object
});
