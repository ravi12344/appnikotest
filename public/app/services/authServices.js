angular.module('authServices', [])

    // Factor: Auth handles all login/logout functions
    .factory('Auth', function($http, AuthToken) {
        var authFactory = {}; // Create the factory object
        // Function to log the user in
        authFactory.login = function(loginData) {
            return $http.post('/api/authenticate', loginData).then(function(data) {
                AuthToken.setToken(data.data.token); // Endpoint will return a token to set
                AuthToken.setName(data.data.name);
                return data;
            });
        };
        //get authrization from sso service.
        authFactory.getAuthtoken = function(code){
            var url = 'https://webaccessqa.kdc.capitalone.com/as/token.oauth2?code='+code+'&grant_type=authorization_code&redirect_uri=http://axon-poc.clouddqt.capitalone.com/getAuth';
            var base64code = window.btoa("ThickClientOAuthwPingSSO4AXON:ThickClientOAuthwPingSSO4AXON_NonProdSECRET");
            var req = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': "Basic "+ base64code,
                }
            }
            return $http(req).then(function(data){
                return data;
            });
        }

        authFactory.getUserInfo = function (authCode){
            var url = 'https://webaccessQA.kdc.capitalone.com/idp/userinfo.openid';

            var req = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': "application/x-www-form-urlencoded",
                    'Authorization': "Bearer " + authCode
                }
            }
            return $http(req).then(function(data){
                console.log("Name is ",data.data.LName , "/userid is ",data.data.userid);
                AuthToken.setName(data.data.LName);
                AuthToken.setUserId(data.data.userid);
                AuthToken.setLoggedInTime(Date());
                return data;
            });
        }

        //Function to get current user name;
        authFactory.getName = function(){
            console.log("authFactory getName function");
            return AuthToken.getName();
        };
        //Function to get current user id;
        authFactory.getUserId = function(){
            console.log("authFactory getUserId function");
            return AuthToken.getUserId();
        };
        // Function to check if user is currently logged in
        authFactory.isLoggedIn = function() {
            // CHeck if token is in local storage
            if (AuthToken.getName()) {
                var date1 = new Date();
                var date2 = new Date(AuthToken.getLoggedInTime());
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffTimes = (timeDiff / (1000 * 3600));
                console.log(diffTimes);
                if (diffTimes< 8){
                    return true; // Return true if in storage
                }
                else{
                    console.log("remove the session");
                    AuthToken.setName();
                    AuthToken.setUserId();
                    AuthToken.setLoggedInTime();
                    return false;
                }
            } else {
                return false; // Return false if not in storage
            }
        };
        //Function to get current user's app name
        authFactory.getAppname = function(eid){
            console.log("authFactory getAppname function parameter eid is ",eid);
            return $http.post('/api/getAppname', eid).then(function(data){
                return data.data.apps;
            })
        }
        // Function to logout the user
        authFactory.logout = function() {
            AuthToken.setName();
            AuthToken.setUserId();
            AuthToken.setLoggedInTime();
            AuthToken.setToken(); // Removes token from local storage
        };

        return authFactory; // Return object
    })

    // Factory: AuthToken handles all token-associated functions
    .factory('AuthToken', function($window) {
        var authTokenFactory = {}; // Create factory object

        // Function to set and remove the token to/from local storage
        authTokenFactory.setToken = function(token) {
            // Check if token was provided in function parameters
            if (token) {
                $window.localStorage.setItem('token', token); // If so, set the token in local storage
            } else {
                $window.localStorage.removeItem('token'); // Otherwise, remove any token found in local storage (logout)
            }
        };

        // Function to retrieve token found in local storage
        authTokenFactory.getToken = function() {
            return $window.localStorage.getItem('token');
        };

        authTokenFactory.setName = function(name) {
            if (name) {
                $window.localStorage.setItem('name', name);
            }else{
                $window.localStorage.removeItem('name');
            }
        };

        authTokenFactory.getName = function() {
            return $window.localStorage.getItem('name');
        };

        authTokenFactory.setUserId = function(userid) {
            console.log("authTokenFactory==(save,remove function)==>",userid);
            if (userid) {
                $window.localStorage.setItem('userid', userid);
            }else{
                $window.localStorage.removeItem('userid');
            }
        };

        authTokenFactory.getUserId = function() {
            console.log("authTokenFactory==(getUserId function)==>",$window.localStorage.getItem('userid'));
            return $window.localStorage.getItem('userid');
        };

        authTokenFactory.setLoggedInTime = function(date){
            console.log(date);
            if (date) {
                $window.localStorage.setItem('date', date);
            }else{
                $window.localStorage.removeItem('date');
            }
        }

        authTokenFactory.getLoggedInTime = function(){
            return $window.localStorage.getItem('date');
        }


        return authTokenFactory; // Return factory object
    })

    // Factory: AuthInterceptors is used to configure headers with token (passed into config, app.js file)
    .factory('AuthInterceptors', function(AuthToken) {
        var authInterceptorsFactory = {}; // Create factory object

        // Function to check for token in local storage and attach to header if so
        authInterceptorsFactory.request = function(config) {
            var token = AuthToken.getToken(); // Check if a token is in local storage
            if (token) config.headers['x-access-token'] = token; //If exists, attach to headers

            return config; // Return config object for use in app.js (config file)
        };

        return authInterceptorsFactory; // Return factory object


    });




