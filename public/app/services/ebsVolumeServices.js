angular.module('ebsVolumeServices', [])
.factory('Volume', function($http,Config) {
    var volumeFactory = {}; // Create the userFactory object
    var url=Config.url;
    
    volumeFactory.getVolumes = function(name, location){
        var tempUrl = url + location + '/aws/volumes;tags.value=ASV'+name+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    volumeFactory.getVolume = function(volumeId){
        var tempUrl = url + '/aws/volumes/'+volumeId+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    }
    return volumeFactory; // Return instanceFactory object
});
