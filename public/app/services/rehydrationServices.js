angular.module('rehydrationServices', [])
.factory('Rehydration', function($http,Config) {
    var rehydrationFactory = {}; // Create the userFactory object
    rehydrationFactory.getStackTemplate = function(stack){
        var tempUrl = "/stack/getStackTemplate";
        return $http.post(tempUrl,stack);
    }
    rehydrationFactory.getAmiVersions = function(tempForVersion){
        var tempUrl = "/stack/getAmiVersions";
        return $http.post(tempUrl,tempForVersion);
    }
    rehydrationFactory.getMapper = function(title){
        var tempUrl = "/stack/getMapper";
        return $http.post(tempUrl,title);
    }
    rehydrationFactory.getGitTemplate = function (url) {
        console.log(url);
        var tempUrl = '/stack/getGitTemplate';
        var tempData = {};
        tempData.url = url;
        return $http.post(tempUrl,tempData);
    }
    rehydrationFactory.checkingTemplate = function (template) {
        var tempUrl = '/stack/checkingTemplate';
        return $http.post(tempUrl,template);
    }
    rehydrationFactory.saveTemplate = function (template) {
        var tempUrl = '/stack/saveTemplate';
        return $http.post(tempUrl,template);
    }
    rehydrationFactory.createStack = function(template){
        var tempUrl = '/stack/createStack';
        return $http.post(tempUrl,template);
    }
    rehydrationFactory.updateStack = function(template){
        var tempUrl = '/stack/updateStack';
        return $http.post(tempUrl,template);
    }
    return rehydrationFactory; // Return instanceFactory object
});
