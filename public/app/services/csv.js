
(function() {
    angular.module('saranyu.common')
        .service('csv', [csv])

    function csv() {
        this.downloadCSV = function(args, file_name) {
            var filename, link;
            var csv = args
            var filename = file_name
            if (csv == null) return;
            if (!csv.match(/^data:text\/csv/i)) {
                csv = 'data:attachment/csv;charset=utf-8,' + csv;
            }
            data = encodeURI(csv);
            link = document.createElement('a');
            document.body.appendChild(link)
            window.URL = window.URL || window.webkitURL;
            link.id = "csvDwnLink";
            link.setAttribute('href', data);
            link.setAttribute('download', filename);
            link.click();
        }

        this.getTag = function(tags, key) {
            var x = tags
            var myvalue = 'missing';
            //console.log("tags:" + JSON.stringify(x));
            if (null === x || typeof x === 'undefined') {} else {
                for (var k = 0; k < x.length; k++) {
                    if (tags[k].Key == key) {
                        var myvalue = tags[k].Value
                    }
                }
            }
            return myvalue
        }

        this.returnArrayAsDashSeperated = function(myarray) {
            var retVal = ''
            if (null === myarray || typeof myarray === 'undefined') {
                return ''
            } else {
                for (var k = 0; k < myarray.length; k++) {
                    retVal += myarray[k] + ' '
                }
                return retVal.slice(0, -6)
                return retVal
            }
        }

        String.prototype.formatString = function(index, character) {
            return this.substr(0, index) + character + this.substr(index + character.length);
        }

        this.returnArrayAsDashSeperatedWithIdentifier = function(myarray, identifier) {
            var retVal = ''
            for (var k = 0; k < myarray.length; k++) {
                retVal += myarray[k][identifier] + ' '
            }
            if (retVal.length > 0) {
                return (retVal.slice(0, -6))
            } else {
                return ''
            }
        }


        this.convertArrayOfObjectsToCSV = function(mydata) {
            var result, ctr, keys, columnDelimiter, lineDelimiter;
            var data = mydata
            columnDelimiter = data.columnDelimiter || ',';
            lineDelimiter = data.lineDelimiter || '\n';
            keys = Object.keys(data[0]);
            result = '';
            result += keys.join(columnDelimiter);
            result += lineDelimiter;
            data.forEach(function(item) {
                ctr = 0;
                keys.forEach(function(key) {
                    if (ctr > 0) result += columnDelimiter;
                    result += JSON.stringify(item[key]);
                    ctr++;
                });
                result += lineDelimiter;
            });
            //console.log(result)
            return result;
        }


    }
})();