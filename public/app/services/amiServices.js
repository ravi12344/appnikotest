angular.module('amiServices', [])
    .factory('Ami', function($http,Config) {
        var amiFactory = {}; // Create the userFactory object
        var url=Config.url;

        amiFactory.getAmiIds = function(name,location){
                var tempUrl = url + location + '/aws/images;tags.value=ASVAWSAMI'+';_pp';
            console.log(tempUrl);
            return $http.get(tempUrl);
        };
        amiFactory.getAmiId = function(amiId){
            var tempUrl = url + '/aws/images/' + amiId + ';_pp';
            console.log(tempUrl);0
            return $http.get(tempUrl);
        }
        return amiFactory; // Return instanceFactory object
    });