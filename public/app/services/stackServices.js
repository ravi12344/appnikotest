angular.module('stackServices', [])
.factory('Stack', function($http,Config) {
    var stackFactory = {}; // Create the userFactory object
    var url=Config.url;
    stackFactory.getInstances = function(name, location){
        var tempUrl = url + location + '/view/instances;tags.value=ASV'+name+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    stackFactory.getInstance = function(instanceId){
        var tempUrl = url + '/view/instances/' + instanceId + ';_pp';
        console.log(tempUrl);
        return $http.get(tempUrl);
    }
    stackFactory.getloadBalancerInstance = function(name){
        var tempUrl = url + '/view/loadBalancerInstances/ASV'+name+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    stackFactory.getStack = function(stackName){
        console.log(url + '/aws/stacks/'+stackName+';_pp');
        return $http.get(url + '/aws/stacks/'+stackName+';_pp');
    };
    stackFactory.getSnapshots = function(tags){
        var tempUrl = url + '/aws/snapshots/;tags.value=' + tags.instanceId  + ';tags.value=' + tags.volumeId; //+ ';tags.value=' + tags.deviceName
        console.log(url + '/aws/snapshots/;tags.value=' + tags.instanceId  + ';tags.value=' + tags.volumeId + ';tags.value=' + tags.deviceName);
        return $http.get(tempUrl);
    };
    stackFactory.getSnapshot = function (snapshotId) {
        var tempUrl = url + '/aws/snapshots/' + snapshotId;
        console.log(tempUrl);
        return $http.get(tempUrl);
    }
    return stackFactory; // Return instanceFactory object
});
