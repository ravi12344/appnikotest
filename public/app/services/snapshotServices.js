angular.module('snapshotServices', [])
.factory('Snapshot', function($http,Config) {
    var snapshotFactory = {}; // Create the userFactory object
    var url=Config.url;
    
    snapshotFactory.getSnapshots = function(name, location){
        var tempUrl = url + location + '/aws/snapshots;tags.value=ASV'+name+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    snapshotFactory.getSnapshot = function(snapshotId){
        var tempUrl = url + '/aws/snapshots/'+snapshotId+';_pp;';
        console.log(tempUrl);
        return $http.get(tempUrl);
    }
    return snapshotFactory; // Return instanceFactory object
});
