angular.module('overviewServices', [])
    .factory('Overview', function($http,Config) {
        var overviewFactory = {}; // Create the userFactory object
        var url=Config.url;
        //Overview.getTotal(currentStatus)instance nuber
        overviewFactory.getRunningEnvInstances = function(name,env,location){
            var state = "running";
            var tempUrl = url + location + '/view/instances;tags.value=ENV'+env+ name + ';state.name=' + state + ';_pp';
            var tempUrl1 = url + location + '/view/instances;tags.value=ASV'+env+ name + ';state.name=' + state + ';_pp';
            if (env){
                return $http.get(tempUrl);
            }else{
                return $http.get(tempUrl1);
            }
        };
        overviewFactory.getStoppedEnvInstances = function(name,env,location){
            var state = "stopped";
            var tempUrl = url + location + '/view/instances;tags.value=ENV'+env+ name + ';state.name=' + state + ';_pp';
            var tempUrl1 = url + location + '/view/instances;tags.value=ASV'+env+ name + ';state.name=' + state + ';_pp';
            if (env){
                return $http.get(tempUrl);
            }else{
                return $http.get(tempUrl1);
            }
        };
        overviewFactory.getTerminatedEnvInstances = function(name,env,location){
            var state = "terminated";
            var tempUrl = url + location + '/view/instances;tags.value=ENV'+env+ name + ';state.name=' + state + ';_pp';
            var tempUrl1 = url + location + '/view/instances;tags.value=ASV'+env+ name + ';state.name=' + state + ';_pp';
            if (env){
                return $http.get(tempUrl);
            }else{
                return $http.get(tempUrl1);
            }
        };
        //Overview.getBuckets();
        overviewFactory.getBuckets = function(name,env,location){
            return $http.get(url+location +'/aws/buckets;tags.value=ENV'+env+ name + ';_pp');
        };
        //Overview.getAutoScaling();
        overviewFactory.getAutoScaling = function(name,env,location){
            if (env){
                return $http.get(url+location +'/aws/autoScalingGroups;tags.value=ENV'+env+ name + ';_pp');
            }else{
                return $http.get(url+location +'/aws/autoScalingGroups;tags.value=ASV'+env+ name + ';_pp');
            }

        }
        overviewFactory.getInUseVolums = function(name,env,location){
            if (env){
                return $http.get(url+location +'/aws/volumes;tags.value=ENV'+env+ name +';state=in-use;_pp;');
            }else{
                return $http.get(url+location +'/aws/volumes;tags.value=ASV'+env+ name +';state=in-use;_pp;');
            }

        };
        overviewFactory.getAvailableVolums = function(name,env,location){
            if (env){
                return $http.get(url+location +'/aws/volumes;tags.value=ENV'+env+ name +';state=available;_pp;');
            }else{
                return $http.get(url+location +'/aws/volumes;tags.value=ASV'+env+ name +';state=available;_pp;');
            }

        };
        overviewFactory.getUnencryptedVolums = function(name,env,location){
            if (env){
                return $http.get(url+location +'/aws/volumes;tags.value=ENV'+env+ name +';state=unencrypted;_pp;');
            }else{
                return $http.get(url+location +'/aws/volumes;tags.value=ASV'+env+ name +';state=unencrypted;_pp;');
            }

        };

        //classic and application
        //clasic ELB: aws/loadBlancers
        //app ELB: aws/loadBlancersV2
        overviewFactory.getAppLoadBalancers = function(name,env,location){
            if (env){
                return $http.get(url+location+'/aws/loadBalancersV2;tags.value=ENV'+env+name+';_pp');
            }else{
                return $http.get(url+location+'/aws/loadBalancersV2;tags.value=ASV'+env+name+';_pp');
            }
        }
        overviewFactory.getClassicLoadBalancers = function(name,env,location){
            if (env){
                return $http.get(url+location+'/aws/loadBalancers;tags.value=ENV'+env+name+';_pp');
            }else{
                return $http.get(url+location+'/aws/loadBalancers;tags.value=ASV'+env+name+';_pp');
            }
        }
        overviewFactory.getSecurityGroups = function(name,env,location){
            if (env){
                return $http.get(url+location+'/aws/securityGroups;tags.value=ENV'+env+name+';_pp');
            }else{
                return $http.get(url+location+'/aws/securityGroups;tags.value=ASV'+env+name+';_pp');
            }
        }
        //-----get EC2 information----------
        overviewFactory.getInstances = function(name,location){
            //console.log(url+location+'/view/instances;tags.value=ASV'+name+';_pp');
            return $http.get(url+location+'/view/instances;tags.value=ASV'+name+';_pp');
        }
        overviewFactory.getEC2info = function(instanceId){
            //console.log(url+'/view/instances/'+instanceId+';_pp;_expand:(imageId,tags,publicIpAddress)');
            return $http.get(url+'/view/instances/'+instanceId+';_pp;_expand:(instanceId,imageId,tags,publicIpAddress)');
        }
        overviewFactory.getImageCreationData = function(imageId){
            //console.log(url+'/aws/images/'+imageId+';_pp;_expand:(creationDate)');
            return $http.get(url+'/aws/images/'+imageId+';_pp;_expand:(creationDate,public,tags)');
            //return $http.get(url+'/aws/images/'+imageId+';_pp;_expand:(creationDate,public)');
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        //------get EBS information-----------
        overviewFactory.getVolumes = function(name,location){
            //console.log(url+location+'/aws/volumes;tags.value=ASV'+name+';_pp');
            return $http.get(url+location+'/aws/volumes;tags.value=ASV'+name+';_pp');
        }
        overviewFactory.getVolumeinfo = function(volumeId){
            //console.log(url+'/aws/volumes/'+volumeId+';_pp;_expand:(tags,encrypted,volumeId);');
            return $http.get(url+'/aws/volumes/'+volumeId+';_pp;_expand:(tags,encrypted,volumeId);');
        }
        overviewFactory.getSnapshot = function(volumeId){
            //console.log(url+'/aws/snapshots;tags.value='+volumeId);
            return $http.get(url+'/aws/snapshots;tags.value='+volumeId);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        //-------get RDS information----------
        overviewFactory.getDatabases = function(name,location){
            //console.log(url+location+'/aws/databases;tags.value=ASV'+name+';_pp');
            return $http.get(url+location+'/aws/databases;tags.value=ASV'+name+';_pp');
        }
        overviewFactory.getDatabaseinfo = function(dbname){
            //console.log(url+'/aws/databases/'+dbname+';_pp;_expand:(tags,storageEncrypted,autoMinorVersionUpgrade,backupRetentionPeriod,publiclyAccessible)');
            return $http.get(url+'/aws/databases/'+dbname+';_pp;_expand:(tags,storageEncrypted,autoMinorVersionUpgrade,backupRetentionPeriod,publiclyAccessible)')
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        //-------get ELB information----------
        overviewFactory.getALBs = function(location){           //function(name,location)
            //console.log(url+location+'/aws/loadBalancersV2;tags.value=ASV'+name+';_pp');
            return $http.get(url+location+'/aws/loadBalancersV2;_pp'); //return $http.get(url+location+'/aws/loadBalancersV2;tags.value=ASV'+name+';_pp');
        }
        overviewFactory.getALBinfo = function(loadBalancer){
            //console.log(url+'/aws/loadBalancersV2/'+loadBalancer+';_PP');
            return $http.get(url+'/aws/loadBalancersV2/'+loadBalancer+';_PP');
        }
        overviewFactory.getCLBs = function(name,location){
            //console.log(url+location+'/aws/loadBalancers;tags.value=ASV'+name+';_pp');
            return $http.get(url+location+'/aws/loadBalancers;tags.value=ASV'+name+';_pp');
        }
        overviewFactory.getCLB = function(loadBalancer){
            //console.log(url+'/aws/loadBalancers/'+loadBalancer+';_PP');
            return $http.get(url+'/aws/loadBalancers/'+loadBalancer+';_PP');
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


        //get asv details
        overviewFactory.getDetails = function(name){
            return $http.post('/api/getAsvDetails',name);
        }
        return overviewFactory; // Return overviewFactory object
    });
//-----EC2-----
//   /view/instances/i-0f7d079f4ceb55597;_pp;_expand:(imageId,tags,publicIpAddress)
//   /aws/images/ami-5e63d13e;_pp;_expand:(creationDate)

//-----EBS-----
//      /aws/volumes/vol-0cd1396792a8b5e01;_pp;_expand:(tags,encrypted,snapshotId,volumeId);
//      /aws/snapshots;tags.value=vol-0cd1396792a8b5e01;

//-----RDS-----
//      /aws/databases/myravidb;_pp;_expand:(tags,storageEncrypted,autoMinorVersionUpgrade,backupRetentionPeriod,NoPublicIp)
