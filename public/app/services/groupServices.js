angular.module('groupServices', [])
.factory('Group', function($http,Config) {
    var groupFactory = {}; // Create the userFactory object
    var url=Config.url;
    
    groupFactory.getInstances = function(name, location){
        var tempUrl = url + location + '/view/instances;tags.value=ASV'+name+';_pp;';
        return $http.get(tempUrl);
    };
    groupFactory.getInstance = function(instanceId){
        var tempUrl = url + '/view/instances/' + instanceId + ';_pp';
        console.log(tempUrl);
        return $http.get(tempUrl);
    };
    groupFactory.getGroups = function(name , location){
        console.log(url + location+ '/aws/autoScalingGroups;tags.value=ASV'+name+';_pp');
        return $http.get(url + location+ '/aws/autoScalingGroups;tags.value=ASV'+name+';_pp');
    }
    groupFactory.getGroup = function(name){
        console.log(url + '/aws/autoScalingGroups/'+name+';_pp');
        return $http.get(url + '/aws/autoScalingGroups/'+name+';_pp');
    }
    groupFactory.getScalingPolicies = function(name){
        console.log(url+'/aws/scalingPolicies;autoScalingGroupName='+name+';_pp');
        return $http.get(url+'/aws/scalingPolicies;autoScalingGroupName='+name+';_pp');
    }

    return groupFactory; // Return instanceFactory object
});
