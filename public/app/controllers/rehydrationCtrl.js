angular.module('rehydrationController',['rehydrationServices'])
    // Controller: User to control the management page and managing of user accounts
    .controller('rehydrationCtrl', function($scope,$rootScope,$routeParams,Rehydration) {
        var app = this;
        var appName = $scope.currentApp;
        $rootScope.$emit("showASGFilter", false);
        $scope.collapsePanel = false;
        $scope.currentPage = 0;
        $scope.pageSize = 20;
        $scope.selectedTemplate = 'git';
        $scope.selectedAction = 'create';
        app.stackName = $routeParams.stackName;
        app.latestSnapshotId = $routeParams.latest_snapshot;
        app.creationTime = $routeParams.creationTime;
        app.state = $routeParams.state;
        app.start_process = false;
        app.fetching_tp_loading = true;
        app.fetching_ami_loading = true;
        app.fetching_snapshot_loading = true;
        app.fetching_latest_ami_loading = true;
        app.fetching_latest_snapshot_loading = true;
        app.updating_value_loading = true;
        app.validating_loading = true;
        app.saving_loading = true;
        app.validating = true;
        app.createStackFailed = false;
        app.updateStackFailed = false;
        app.createStackSuccess = false;
        app.updateStackSuccess = false;
        var getFilter = function(){
            //east checked
            if ($scope.east){
                app.location = '/act.us-east-1';
            }
            //west checked
            if ($scope.west){
                app.location = '/act.us-west-2';
            }
            //all checked
            if ($scope.east && $scope.west){
                app.location = '';
            }
            if (!$scope.east && !$scope.west){
                app.location = '';
            }
        }
        var init = function(){
            console.log("init");
            var tempStack = {};
            tempStack.stackName = app.stackName;
            tempStack.region = 'us-east-1';
            app.stack=tempStack;

            Rehydration.getStackTemplate(app.stack).then(function(data){
                app.obj = data.data.obj;
                var title = data.data.title;
                var tempData = {};
                tempData.title = data.data.title;
                Rehydration.getMapper(tempData).then(function(mapper){
                    var amis = mapper.data.amis;
                    var snapshots = mapper.data.snapshots;
                    app.cft = mapper.data.cft;
                    app.snapshot = snapshots[0].deviceName;
                    //app.snapId = snapshots[0].currentSnapId;
                    var currentAmiIdPath = amis[0].currentAmiId;
                    app.ami_path = currentAmiIdPath.split(".");
                    var currentAmiId = app.obj;
                    for (var i=0 ; i<app.ami_path.length;i++){
                        currentAmiId = currentAmiId[app.ami_path[i]];
                    }
                    app.currentAmiId = currentAmiId;
                    var currentSnapshot = app.obj;
                    app.snap_path = snapshots[0].currentSnapId.split(".");
                    for (var i=0 ; i<app.snap_path.length;i++){
                        currentSnapshot = currentSnapshot[app.snap_path[i]];
                    }
                    app.currentSnapshot = currentSnapshot;
                    var tempForVersion = {};
                    tempForVersion.latestAmiLink = amis[0].latestAmiLink;
                    tempForVersion.accountName = amis[0].accountName;
                    app.amiFlavor = amis[0].sysType;
                    tempForVersion.regionName = amis[0].regionName;
                    Rehydration.getAmiVersions(tempForVersion).then(function(data){
                        app.latestAmiId = data.data.latestAmi;
                        app.amiVersion = data.data.versions[0];
                        app.amiVersions = data.data.versions;
                        app.amiIds = data.data.amiIds;
                    })
                });
            });
        }

        app.proceed = function(){
            //get template from github or aws template.
            app.fetching_tp_loading = true;
            app.fetching_ami_loading = true;
            app.fetching_snapshot_loading = true;
            app.fetching_latest_ami_loading = true;
            app.fetching_latest_snapshot_loading = true;
            app.updating_value_loading = true;
            app.validating_loading = true;
            app.saving_loading = true;
            app.validating = true;
            app.createStackFailed = false;
            app.updateStackFailed = false;
            app.createStackSuccess = false;
            app.updateStackSuccess = false;
            app.emptyStackName = false;
            app.start_process = true;
            if ($scope.selectedTemplate=='git'){
                console.log("git option clicked");
                $scope.processMsg += $scope.processMsg + 'fetching template from Git now' + '\n';
                // app.cft = 'https://raw.githubusercontent.com/ravigawade/Project12/master/testjson.json'; //for testing
                Rehydration.getGitTemplate(app.cft).then(function(data){
                    app.fetching_tp_loading = false;
                    app.fetching_ami_loading = false;
                    app.fetching_snapshot_loading = false;
                    app.fetching_latest_ami_loading = false;
                    app.fetching_latest_snapshot_loading = false;
                    console.log(data.data.template);
                    var template = data.data.template;
                    template[app.ami_path[0]][app.ami_path[1]][app.ami_path[2]][app.ami_path[3]]=app.latestAmiId;
                    template[app.snap_path[0]][app.snap_path[1]][app.snap_path[2]][app.snap_path[3]]=app.currentSnapshot;
                    app.updating_value_loading = false;
                    //checking template validation.
                    Rehydration.checkingTemplate(template).then(function(data){
                        console.log("checking Template = ",data.data);
                        if (data.data.success == true){
                            app.validating_loading = false;
                            $scope.validateStack = true;
                            console.log("Validate Template");
                            if ($scope.selectedAction=='create'){
                                app.createStack(template);
                            }else if ($scope.selectedAction=='update'){
                                app.updateStack(template);
                            }
                        }else{
                            app.validating = false;
                        }
                    });
                    //save template to s3.
                });
            }else{
                app.fetching_tp_loading = false;
                app.fetching_ami_loading = false;
                app.fetching_snapshot_loading = false;
                app.fetching_latest_ami_loading = false;
                app.fetching_latest_snapshot_loading = false;
                var template = app.obj;
                template[app.ami_path[0]][app.ami_path[1]][app.ami_path[2]][app.ami_path[3]]=app.latestAmiId;
                template[app.snap_path[0]][app.snap_path[1]][app.snap_path[2]][app.snap_path[3]]=app.currentSnapshot;
                app.updating_value_loading = false;
                Rehydration.checkingTemplate(template).then(function(data){
                    console.log("checking Template = ",data.data);
                    if (data.data.success == true){
                        app.validating_loading = false;
                        console.log("Validate Template");
                        if ($scope.selectedAction=='create'){
                            app.createStack(template);
                        }else if ($scope.selectedAction=='update'){
                            app.updateStack(template);
                        }
                    }else{
                        app.validating = false;
                    }
                });
            }

            //create stack or update stack.
        }
        app.createStack = function(template){
            if ($scope.newStackName ==""){
                app.emptyStackName = true;
            }else{
                $scope.processMsg += $scope.processMsg + 'creating new stack name is ' + $scope.newStackName + '\n';
                var temp={};
                temp.stackName = $scope.newStackName;
                temp.obj = template;
                Rehydration.createStack(temp).then(function(data){
                    if (data.data.success = true){
                        app.saving_loading = false;
                        console.log('create stack');
                        app.createStackSuccess = true;

                    }else{
                        app.createStackFailed = true;

                    }
                })
            }
        }
        app.updateStack = function(template){
            var temp={};
            temp.stackName = app.stackName;
            temp.obj = template;
            $scope.processMsg += $scope.processMsg + 'stack is updating now...' + '\n';
            Rehydration.updateStack(temp).then(function(data){
                if (data.data.success= true){
                    app.saving_loading = false;
                    console.log('update stack');
                    app.updateStackSuccess = true;

                }else{
                    app.updateStackFailed = true;
                }
            })
        }

        $scope.getSelectedAmiId = function(){
            var idx = app.amiVersions.indexOf(app.amiVersion);
            app.latestAmiId = app.amiIds[idx];
        }
        init();

    })