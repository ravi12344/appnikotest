angular.module('groupController', ['groupServices'])

// Controller: User to control the management page and managing of user accounts
.controller('groupCtrl', function($scope,$rootScope,User,Group) {
    var app = this;
    var appName = $scope.currentApp;
    $rootScope.$emit("showASGFilter", false);
    $scope.collapsePanel = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    var getFilter = function(){
        //east checked
        if ($scope.east){
            app.location = '/act.us-east-1';
        }
        //west checked
        if ($scope.west){
            app.location = '/act.us-west-2';
        }
        //all checked
        if ($scope.east && $scope.west){
            app.location = '';  
        }

        if (!$scope.east && !$scope.west){
            app.location = '';
        }
        groups();
    }
    var groups = function(){
        console.log("-------------------get Groups--------------------");
        var appName = $scope.currentApp;
        app.loading = true;
        Group.getGroups(appName,app.location).then(function(data){
            app.groupIds = data.data;
            app.groups = [];
            app.outgroups = [];
            app.loading = true;
            var getDataCompleted = 0;
            for (index in app.groupIds){
                console.log("groupid->",app.groupIds[index]);
                Group.getGroup(app.groupIds[index]).then(function(group_data){
                    console.log(group_data.data);
                    app.groups.push(group_data.data);
                    var tags = group_data.data.tags;
                    if (app.npprod){
                        for (idx in tags){
                            if (tags[idx].value.indexOf("ENVNP")>-1){
                                app.outgroups.push(group_data.data);
                            }
                        }
                    }
                    if (app.prprod){
                        for (idx in tags){
                            if (tags[idx].value.indexOf("ENVPR")>-1){
                                app.outgroups.push(group_data.data);
                            }
                        }
                    }
                    if (app.prod){
                        for (idx in tags){
                            if (tags[idx].value.indexOf("ENVPP")>-1){
                                app.outgroups.push(group_data.data);
                            }
                        }
                    }
                    if (!app.npprod && !app.prprod && !app.prod){
                        app.outgroups.push(group_data.data);
                    }
                    getDataCompleted++;
                    if (getDataCompleted == app.groupIds.length)
                    {
                        app.loading = false;
                    }

                });
            }
            if (app.groupIds.length==0){
                app.loading=false;
            }
        });
    }
    app.setCurrentGroup = function(item){
        console.log("set currentGroup");
        app.currentPolicies = [];
        app.currentGroupSubnets = [];
        app.currentGroupSubnets = item.VPCZoneIdentifier.split(",");
        console.log(app.currentGroupSubnets);
        Group.getScalingPolicies(item.autoScalingGroupName).then(function(data){
            app.currentPolicies = data.data;
            console.log(app.currentPolicies);
            app.currentGroup = item;
        });
        $scope.collapsePanel = true;
    }
    $scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
        getFilter();
    });
    $scope.setEditMode = function(){
        $scope.inputMode=false;
        $scope.editMode=!$scope.editMode;
        $scope.createMode = false;
    }
    $scope.setCreateMode = function(){
        $scope.inputMode=false;
        $scope.createMode = !$scope.createMode;
        $scope.key = '';
        $scope.value = '';

        if ($scope.editMode==true && $scope.createMode==true){
            $scope.inputMode=true;
        }else
        {
            $scope.inputMode=false;
        }
    }
    $scope.numberOfPages = function(){
        if (app.outgroups){
            return Math.ceil(app.outgroups.length/$scope.pageSize);
        }else{
            return 1;
        }
        
    }
    

})
.filter('startFrom',function(){
    return function(input,start){
        start = +start;
        if (input)
        {
            return input.slice(start);
        }else{
            return null;
        }
        
    }
})