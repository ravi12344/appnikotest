angular.module('overviewController', ['overviewServices','meterGauge'])
    .directive('exportToCsv',function(){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var el = element[0];
                element.bind('click', function(e){

                    var str="ASV"+scope.currentApp+",,,,,"+"\n";
                    str+=",,,,,, \n";
                    str+="Service Owner:,"+",,,,,"+"\n";
                    str+=",,,,,, \n";
                    str+="Total Complaincy:,"+scope.classic.needleVal+"%"+",,,,,"+"\n";
                    str+=",,,,,, \n";
                    str+="EC2"+",,,,,,"+"\n";
                    str+="Tags,Encryption,AMI,NoPublicIP,,,"+"\n";
                    str+=scope.ec_tags+"%"+","+scope.ec_encryption+"%"+","+scope.ec_ami+"%"+","+scope.ec_nopublicip+"%"+",,,"+"\n";
                    str+=",,,,,, \n";
                    str+="InstanceID,InstanceName,Tags,Encryption,AMI,NoPublicIP, \n";
                    var array = scope.instanceLists;
                    for (var i = 0; i < array.length; i++) {
                        var line = '';
                        line=array[i].instanceName + ',' + array[i].instanceId + ',' + array[i].tags + ',' + array[i].nopublicip + ',' + array[i].ami + ',' + array[i].encryption + ",";
                        str += line + '\n';
                    }
                    str+=",,,,,, \n";
                    str+="EBS,,,,,, \n";
                    str+="Tags,Encryption,Backups,,,,\n";
                    str+=scope.ebs_tags + "%" +","+ scope.ebs_encryption +"%"+","+ scope.ebs_backups + "%"+",,,,\n";
                    str+=",,,,,, \n";
                    str+="RDS,,,,,, \n";
                    str+="Tags,Encryption,Backups,RDSpatching,NopublicIp,,\n";
                    str+=scope.rds_tags + "%"+","+ scope.rds_encryption + "%"+","+scope.rds_backups+ "%"+","+ scope.rds_patching+ "%"+","+ scope.rds_nopublicip+"%"+",,\n";
                    str+=",,,,,, \n";
                    str+="DBInstance,DBName,Tags,Encryption,Backups,RDSpatching,NopublicIp \n";

                    var a = $('<a/>', {
                        style:'display:none',
                        href:'data:application/octet-stream;base64,'+btoa(str),
                        download:'Compliance Report.csv'
                    }).appendTo('body')
                    a[0].click()
                    a.remove();
                });
            }
        }
    })
    // Controller: User to control the management page and managing of user accounts
    .controller('overviewCtrl', function($scope,$rootScope,User,Overview,$timeout) {
        var app = this;
        app.appName = 'ASV'+$scope.currentApp;
        $rootScope.$emit("showASGFilter", false);
        $scope.main_show=true;
        $scope.classic = {
            gaugeRadius: 150,
            minVal: 0,
            maxVal: 100,
            needleVal: Math.round(0),
            tickSpaceMinVal: 5,
            tickSpaceMajVal: 10,
            divID: "gaugeBox",
            gaugeUnits: "%",
            tickColMaj:'#656D78',
            tickColMin:'#656D78',
            outerEdgeCol:'#2F4F4F',
            pivotCol:'#000000',
            innerCol:'#F5FFFA',
            unitsLabelCol:'#000000',
            tickLabelCol:'#0000000',
            needleCol:'#aaa',
            defaultFonts:''
        }
        app.filterHide=false;
        app.ecloading = false;
        app.envInstances = {};
        app.ecprogress = 0;
        app.appelbprogress =0;
        app.elbprogress =0;
        app.rdsprogress =0;
        app.ebsprogress =0;
        app.speedometerValue = 0;
        app.speedometerTarget = 10;
        app.speedometerComp = false;
        app.setHide = function(){
            app.filterHide = true;
        }
        app.setShow = function(){
            app.filterHide = false;
        }
        //get running indstance number
        var getFilter = function(){
            app.appName = 'ASV'+$scope.currentApp;
            //east checked
            if ($scope.east){
                app.location = '/act.us-east-1';
            }
            //west checked
            if ($scope.west){
                app.location = '/act.us-west-2';
            }
            //all checked
            if ($scope.east && $scope.west){
                app.location = '';
            }

            if (!$scope.east && !$scope.west){
                app.location = '';
            }


        }
        var getInstanceCount = function(){
            var tempEnvInstances = {
                running : 0,
                stopped : 0,
                terminated : 0
            };
            app.envInstances=tempEnvInstances;
            var appName = $scope.currentApp;
            // if ($scope.npprod){
            // 	app.ecloading = true;
            // 	console.log(app.location);
            // 	Overview.getRunningEnvInstances(appName,"NP",app.location).then(function(data){
            // 		tempEnvInstances.running = data.data.length;
            // 	});
            // 	Overview.getStoppedEnvInstances(appName,"NP",app.location).then(function(data){
            // 		tempEnvInstances.stopped = data.data.length;
            // 	});
            // 	Overview.getTerminatedEnvInstances(appName,"NP",app.location).then(function(data){
            // 		tempEnvInstances.terminated = data.data.length;
            // 		app.ecloading = false;
            // 	});
            // 	app.envInstances=tempEnvInstances;
            // }
            // if ($scope.prprod){
            // 	app.ecloading = true;
            // 	Overview.getRunningEnvInstances(appName,"PP",app.location).then(function(data){
            // 		tempEnvInstances.running += data.data.length;
            // 	});
            // 	Overview.getStoppedEnvInstances(appName,"PP",app.location).then(function(data){
            // 		tempEnvInstances.stopped += data.data.length;
            // 	});
            // 	Overview.getTerminatedEnvInstances(appName,"PP",app.location).then(function(data){
            // 		tempEnvInstances.terminated += data.data.length;
            // 		app.ecloading = false;
            // 	});
            // 	app.envInstances=tempEnvInstances;
            // }
            // if ($scope.prod){
            // 	app.ecloading = true;
            // 	Overview.getRunningEnvInstances(appName,"PR",app.location).then(function(data){
            // 		tempEnvInstances.running += data.data.length;
            // 	});
            // 	Overview.getStoppedEnvInstances(appName,"PR",app.location).then(function(data){
            // 		tempEnvInstances.stopped += data.data.length;
            // 	});
            // 	Overview.getTerminatedEnvInstances(appName,"PR",app.location).then(function(data){
            // 		tempEnvInstances.terminated += data.data.length;
            // 		app.ecloading = false;
            // 	});
            // 	app.envInstances=tempEnvInstances;
            // }
            // if (!$scope.npprod && !$scope.prprod && !$scope.prod){
            app.ecloading = true;
            Overview.getRunningEnvInstances(appName,"",app.location).then(function(data){
                tempEnvInstances.running += data.data.length;

            });
            Overview.getStoppedEnvInstances(appName,"",app.location).then(function(data){
                tempEnvInstances.stopped += data.data.length;
            });
            Overview.getTerminatedEnvInstances(appName,"",app.location).then(function(data){
                tempEnvInstances.terminated += data.data.length;
                app.ecloading = false;
            });
            app.envInstances=tempEnvInstances;
            // }
        }
        var getBuckets = function(){
            app.buckets = [];
            var appName = $scope.currentApp;

            if ($scope.npprod){
                app.sloading = true;
                Overview.getBuckets(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.buckets.indexOf(data.data[index])<0){
                            app.buckets.push(data.data[index]);
                        }
                    }
                    app.sloading = false;
                });
            }
            if ($scope.prprod){
                app.sloading = true;
                Overview.getBuckets(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.buckets.indexOf(data.data[index])<0){
                            app.buckets.push(data.data[index]);
                        }
                    }
                    app.sloading = false;
                });
            }
            if ($scope.prod){
                app.sloading = true;
                Overview.getBuckets(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.buckets.indexOf(data.data[index])<0){
                            app.buckets.push(data.data[index]);
                        }
                    }
                    app.sloading = false;
                });
            }
            if (!$scope.npprod && !$scope.prprod && !$scope.prod){
                app.sloading = true;
                Overview.getBuckets(appName,"",app.location).then(function(data){
                    console.log(data.data);
                    for (index in data.data){
                        if (app.buckets.indexOf(data.data[index])<0){
                            app.buckets.push(data.data[index]);
                        }
                    }
                    app.sloading = false;
                });

            }

        }
        var getAutoScalingGroups = function(){
            app.autoScalingGroups = [];
            var appName = $scope.currentApp;

            if ($scope.npprod){
                app.autoloading = true;
                Overview.getAutoScaling(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.autoScalingGroups.indexOf(data.data[index])<0){
                            app.autoScalingGroups.push(data.data[index]);
                        }
                    }
                    app.autoloading = false;
                });
            }
            if ($scope.prprod){
                app.autoloading = true;
                Overview.getAutoScaling(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.autoScalingGroups.indexOf(data.data[index])<0){
                            app.autoScalingGroups.push(data.data[index]);
                        }
                    }
                    app.autoloading = false;
                });
            }
            if ($scope.prod){
                app.autoloading = true;
                Overview.getAutoScaling(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        if (app.autoScalingGroups.indexOf(data.data[index])<0){
                            app.autoScalingGroups.push(data.data[index]);
                        }
                    }
                    app.autoloading = false;
                });
            }
            if (!$scope.npprod && !$scope.prprod && !$scope.prod){
                app.autoloading = true;
                Overview.getAutoScaling(appName,"",app.location).then(function(data){
                    console.log("autoScalingGroups->",data.data);
                    for (index in data.data){
                        if (app.autoScalingGroups.indexOf(data.data[index])<0){
                            app.autoScalingGroups.push(data.data[index]);
                        }
                    }
                    app.autoloading = false;
                });
            }

        }
        var getVolums = function(){
            var tempEnvVolums = {
                inuse : 0,
                available : 0,
                unencrypted : 0
            };
            app.envVolumes=tempEnvVolums;
            var appName = $scope.currentApp;
            // if ($scope.npprod){
            // 	app.voloading = true;
            // 	Overview.getInUseVolums(appName,"NP",app.location).then(function(data){
            // 		tempEnvVolums.inuse = data.data.length;
            // 		console.log("volume->",tempEnvVolums.inuse);
            // 	});
            // 	Overview.getAvailableVolums(appName,"NP",app.location).then(function(data){
            // 		tempEnvVolums.available = data.data.length;
            // 		console.log(data.data);
            // 	});
            // 	Overview.getUnencryptedVolums(appName,"NP",app.location).then(function(data){
            // 		tempEnvVolums.unencrypted = data.data.length;
            // 		app.voloading = false;
            // 	});
            // 	app.envVolumes=tempEnvVolums;
            // }
            // if ($scope.prprod){
            // 	app.voloading = true;
            // 	Overview.getInUseVolums(appName,"PP",app.location).then(function(data){
            // 		tempEnvVolums.inuse += data.data.length;
            // 	});
            // 	Overview.getAvailableVolums(appName,"PP",app.location).then(function(data){
            // 		tempEnvVolums.available += data.data.length;
            // 	});
            // 	Overview.getUnencryptedVolums(appName,"PP",app.location).then(function(data){
            // 		tempEnvVolums.unencrypted += data.data.length;
            // 		app.voloading = false;
            // 	});
            // 	app.envVolumes=tempEnvVolums;
            // }
            // if ($scope.prod){
            // 	app.voloading = true;
            // 	Overview.getInUseVolums(appName,"PR",app.location).then(function(data){
            // 		tempEnvVolums.inuse += data.data.length;
            // 	});
            // 	Overview.getAvailableVolums(appName,"PR",app.location).then(function(data){
            // 		tempEnvVolums.available += data.data.length;
            // 	});
            // 	Overview.getUnencryptedVolums(appName,"PR",app.location).then(function(data){
            // 		tempEnvVolums.unencrypted += data.data.length;
            // 		app.voloading = false;
            // 	});
            // 	app.envVolumes=tempEnvVolums;
            // }
            // if (!$scope.npprod && !$scope.prprod && !$scope.prod){
            // 	app.voloading = true;
            Overview.getInUseVolums(appName,"",app.location).then(function(data){
                tempEnvVolums.inuse += data.data.length;
            });
            Overview.getAvailableVolums(appName,"",app.location).then(function(data){
                tempEnvVolums.available += data.data.length;
            });
            Overview.getUnencryptedVolums(appName,"",app.location).then(function(data){
                tempEnvVolums.unencrypted += data.data.length;
                app.voloading = false;
            });
            app.envVolumes=tempEnvVolums;
            // }
        }
        var getDatabases = function(){
            app.rdsloading = true;
            app.databases = [];
            Overview.getDatabases().then(function(data){
                app.databases = data.data;
                app.rdsloading = false;
            });
        }
        var getAppLoadBalancers = function(){
            app.albloading = true;
            app.apploadBalancers = [];
            if ($scope.npprod){
                app.albloading = true;
                Overview.getAppLoadBalancers(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        app.apploadBalancers.push(data.data[index]);
                    }
                    app.albloading = false;
                });
            }
            if ($scope.prprod){
                app.albloading = true;
                Overview.getAppLoadBalancers(appName,"PP",app.location).then(function(data){
                    for (index in data.data){
                        app.apploadBalancers.push(data.data[index]);
                    }
                    app.albloading = false;
                });
            }
            if ($scope.prod){
                app.albloading = true;
                Overview.getAppLoadBalancers(appName,"PR",app.location).then(function(data){
                    for (index in data.data){
                        app.apploadBalancers.push(data.data[index]);
                    }
                    app.albloading = false;
                });
            }
            if (!$scope.npprod && !$scope.prprod && !$scope.prod){
                app.albloading = true;
                Overview.getAppLoadBalancers(appName,"",app.location).then(function(data){
                    for (index in data.data){
                        app.apploadBalancers.push(data.data[index]);
                    }
                    app.albloading = false;
                });
            }
        }
        var getClassicLoadBalancers = function(){
            app.clbloading = true;
            app.classicloadBalancers = [];
            if ($scope.npprod){
                app.clbloading = true;
                Overview.getClassicLoadBalancers(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        app.classicloadBalancers.push(data.data[index]);
                    }
                    app.clbloading = false;
                });
            }
            if ($scope.prprod){
                app.clbloading = true;
                Overview.getClassicLoadBalancers(appName,"PP",app.location).then(function(data){
                    for (index in data.data){
                        app.classicloadBalancers.push(data.data[index]);
                    }
                    app.clbloading = false;
                });
            }
            if ($scope.prod){
                app.clbloading = true;
                Overview.getClassicLoadBalancers(appName,"PR",app.location).then(function(data){
                    for (index in data.data){
                        app.classicloadBalancers.push(data.data[index]);
                    }
                    app.clbloading = false;
                });
            }
            if (!$scope.npprod && !$scope.prprod && !$scope.prod){
                app.clbloading = true;
                Overview.getClassicLoadBalancers(appName,"",app.location).then(function(data){
                    for (index in data.data){
                        app.loadBalancers.push(data.data[index]);
                    }
                    app.clbloading = false;
                });
            }
        }
        var getSecurityGroups = function(){
            app.sgloading = true;
            app.securityGroups = [];
            var appName = $scope.currentApp;
            if ($scope.npprod){
                app.sgloading = true;
                Overview.getSecurityGroups(appName,"NP",app.location).then(function(data){
                    for (index in data.data){
                        app.securityGroups.push(data.data[index]);
                    }
                    app.sgloading = false;
                });
            }
            if ($scope.prprod){
                app.sgloading = true;
                Overview.getSecurityGroups(appName,"PP",app.location).then(function(data){
                    for (index in data.data){
                        app.securityGroups.push(data.data[index]);
                    }
                    app.sgloading = false;
                });
            }
            if ($scope.prod){
                app.sgloading = true;
                Overview.getSecurityGroups(appName,"PR",app.location).then(function(data){
                    for (index in data.data){
                        app.securityGroups.push(data.data[index]);
                    }
                    app.sgloading = false;
                });
            }
            if (!$scope.npprod && !$scope.prprod && !$scope.prod){
                app.sgloading = true;
                Overview.getSecurityGroups(appName,"",app.location).then(function(data){
                    for (index in data.data){
                        app.securityGroups.push(data.data[index]);
                    }
                    app.sgloading = false;
                });
            }
        }

        var getEC2info = function(){
            console.log("fetching instance information");
            $scope.instanceLists = [];
            var appName = $scope.currentApp;
            var tag_count = 0;
            var encrytion_count = 0;
            var nopublicip_count = 0;
            var ami_count = 0;
            var total_count = 0;
            var progress = 0;
            // app.instance_lists = {};
            // app.instance_lists.instanceIds = [];
            // app.instance_lists.name = [];
            // app.instance_lists.tags = [];
            // app.instance_lists.nopublicip = [];
            // app.instance_lists.ami = [];
            // app.instance_lists.encryption = [];
            $scope.ec_tags = 0;
            $scope.ec_nopublicip = 0;
            $scope.ec_ami = 0;
            app.ecprogress = 0;
            Overview.getInstances(appName,app.location).then(function(data){
                instanceIds=data.data;
                total_count = data.data.length;
                for (index in instanceIds){
                    Overview.getEC2info(instanceIds[index]).then(function(datas){
                        var temp={};
                        var temp_tags=true;
                        var temp_public=true;
                        var temp_ami=true;
                        var temp_encrytion=true;
                        var temp_name = getInstanceName(datas.data.tags);
                        console.log(datas.data);
                        var temp_id = datas.data.instanceId;
                        if (datas.data.tags.length>0) {
                            tag_count++;
                            $scope.ec_tags = Math.round(tag_count / total_count* 10000)/100 ;
                            temp_tags=true;
                        }else{
                            temp_tags=false;
                        }
                        if (datas.data.publicIpAddress==null){
                            nopublicip_count++;
                            $scope.ec_nopublicip = Math.round(nopublicip_count /total_count* 10000)/100 ;
                            temp_public=true;
                        }else{
                            temp_public=false;
                        }
                        Overview.getImageCreationData(datas.data.imageId).then(function(imageData){
                            if (getDiffDay(imageData.data.creationDate)<60){
                                ami_count++;
                                $scope.ec_ami = Math.round(ami_count / total_count* 10000)/100 ;
                                temp_ami=true;
                            }else{
                                temp_ami=false;
                            }
                            console.log(imageData.data.tags);
                            if (app.isEncrypted(imageData.data.tags) == true){
                                encrytion_count++;
                                $scope.ec_encryption = Math.round(encrytion_count / total_count * 10000)/100 ;
                                temp_encrytion=true;
                            }else{
                                temp_encrytion=false;
                            }
                            if (temp_tags==false || temp_public==false || temp_ami==false || temp_encrytion == false){
                                temp.instanceName = temp_name;
                                temp.instanceId = temp_id;
                                temp.tags = temp_tags;
                                temp.nopublicip = temp_public;
                                temp.ami = temp_ami;
                                temp.encryption = temp_encrytion;
                                console.log(temp_name,temp_id);
                                $scope.instanceLists.push(temp);
                            }
                            progress++;
                            app.ecprogress = (progress)/total_count*100;
                        });
                    });
                }
            });
        }
        app.isEncrypted = function(data){
            console.log(data);
            var isEnc = false;
            for (index in data){
                console.log(data[index].key,data[index].value);
                if (data[index].key=="ASV" && data[index].value=="ASVAWSAMI"){
                    isEnc = true;
                    break;
                }
            }
            return isEnc;
        }
        app.getInstance_list = function(){
            // $scope.instanceLists = [];
            // for (var i=0 ; i<app.instance_lists.instanceIds.length;i++){
            // 	if (app.instance_lists.tags[i]==false || app.instance_lists.nopublicip[i]==false || app.instance_lists.ami[i]==false || app.instance_lists.encryption[i]==false){
            // 		var temp ={};
            // 		temp.instanceName = app.instance_lists.name[i];
            // 		temp.instanceId = app.instance_lists.instanceIds[i];
            // 		temp.tags = app.instance_lists.tags[i];
            // 		temp.nopublicip = app.instance_lists.nopublicip[i];
            // 		temp.ami = app.instance_lists.ami[i];
            // 		temp.encryption = app.instance_lists.encryption[i];
            // 		$scope.instanceLists.push(temp);
            // 	}
            // }
            console.log($scope.instanceLists);
        }


        var getInstanceName = function(tags){
            for (index in tags){
                if (tags[index].key=="Name"){
                    return tags[index].value;
                    break;
                }
            }
        }
        var getEbsinfo = function(){
            console.log("fetching ebs information");
            var appName = $scope.currentApp;
            var tag_count = 0;
            var encrytion_count = 0;
            var backups = 0;
            var total_count = 0;
            var progress=0;
            $scope.ebs_totalCount = 0;
            $scope.ebs_tags = 0;
            $scope.ebs_encryption = 0;
            $scope.ebs_backups = 0;
            app.ebsprogress =0;
            Overview.getVolumes(appName,app.location).then(function(data){
                var volumeIds = data.data;
                total_count = data.data.length;
                $scope.ebs_totalCount = total_count;
                for (index in volumeIds){
                    Overview.getVolumeinfo(volumeIds[index]).then(function(datas){
                        if (datas.data.tags.length>0) {
                            tag_count++;
                            $scope.ebs_tags = Math.round(tag_count / total_count * 10000)/100;
                        }
                        if (datas.data.encrypted == true){
                            encrytion_count ++;
                            $scope.ebs_encryption = Math.round(encrytion_count / total_count * 10000)/100;
                        }
                        progress++;
                        app.ebsprogress =(progress)/total_count * 100;
                    });
                    Overview.getSnapshot(volumeIds[index]).then(function(snapData){
                        if (snapData.data.length>0){
                            backups++;
                            $scope.ebs_backups = Math.round(backups / total_count * 10000)/100;
                        }
                    })
                }

            })
        }

        var getRdsinfo = function(){
            console.log("fetching rds information");
            var appName = $scope.currentApp;
            var total_count = 0;
            var tag_count = 0;
            var encrytion_count = 0;
            var nopublicip_count = 0;
            var patching_count = 0;
            var backups_count = 0;
            var progress = 0;
            app.rds_lists = {};
            app.rds_lists.rdsIds = [];
            app.rds_lists.tags = [];
            app.rds_lists.encryption = [];
            app.rds_lists.patching = [];
            app.rds_lists.backups = [];
            app.rds_lists.nopublicip = [];
            $scope.rds_totalCount = 0;
            $scope.rds_tags =0;
            $scope.rds_encryption = 0;
            $scope.rds_patching = 0;
            $scope.rds_backups = 0;
            $scope.rds_nopublicip = 0;
            app.rdsprogress =0;
            Overview.getDatabases(appName,app.location).then(function(data){
                total_count = data.data.length;
                $scope.rds_totalCount = total_count;
                app.rds_lists.rdsIds = data.data;
                var databases = data.data;
                for (index in databases){
                    Overview.getDatabaseinfo(databases[index]).then(function(dbData){
                        if (dbData.data.tags.length>0) {
                            tag_count++;
                            $scope.rds_tags = Math.round(tag_count / total_count * 10000)/100;
                            app.rds_lists.tags.push(true);
                        }else{
                            app.rds_lists.tags.push(false);
                        }
                        if (dbData.data.storageEncrypted == true){
                            encrytion_count++;
                            $scope.rds_encryption = Math.round(encrytion_count /total_count * 10000)/100;
                            app.rds_lists.encryption.push(true);
                        }else{
                            app.rds_lists.encryption.push(false);
                        }
                        if (dbData.data.autoMinorVersionUpgrade == true){
                            patching_count++;
                            $scope.rds_patching = Math.round(patching_count / total_count * 10000)/100;
                            app.rds_lists.patching.push(true);
                        }else{
                            app.rds_lists.patching.push(false);
                        }
                        if (dbData.data.backupRetentionPeriod>0){
                            backups_count++;
                            $scope.rds_backups = Math.round(backups_count / total_count * 10000)/100;
                            app.rds_lists.backups.push(true);
                        }else{
                            app.rds_lists.backups.push(false);
                        }
                        if (dbData.data.publiclyAccessible==false){
                            nopublicip_count++;
                            $scope.rds_nopublicip = Math.round(nopublicip_count / total_count * 10000)/100;
                            app.rds_lists.nopublicip.push(true);
                        }else{
                            app.rds_lists.nopublicip.push(false);
                        }
                        progress++;
                        app.rdsprogress =(progress)/total_count*100;;
                    })
                }
            });
        }
        app.getRds_list = function(){
            $scope.rdsLists = [];
            for (var i=0 ; i<app.rds_lists.rdsIds.length;i++){
                if (app.rds_lists.tags[i]==false || app.rds_lists.encryption[i]==false || app.rds_lists.patching[i]==false || app.rds_lists.backups[i] == false || app.rds_lists.nopublicip[i] ==false){
                    var temp ={};
                    temp.rdsId = app.rds_lists.rdsIds[i];
                    temp.tags = app.rds_lists.tags[i];
                    temp.encryption = app.rds_lists.encryption[i];
                    temp.patching = app.rds_lists.patching[i];
                    temp.backups = app.rds_lists.backups[i];
                    temp.nopublicip = app.rds_lists.nopublicip[i];
                    $scope.rdsLists.push(temp);
                }
            }
            console.log($scope.rdsLists);
        }

        var speedometerVal = function(){
            // console.log("get value~~~");
            // console.log(app.speedometerValue , app.speedometerTarget);
            var total_count = 4;
            if ($scope.rds_totalCount>0){
                total_count+=5;
            }
            if ($scope.ebs_totalCount>0){
                total_count+=3;
            }

            if ((app.rdsprogress==100 || $scope.rds_totalCount==0) && app.ebsprogress==100 && app.ecprogress==100){
                app.speedometerTarget =  Math.round(($scope.rds_tags + $scope.rds_encryption + $scope.rds_patching + $scope.rds_backups + $scope.rds_nopublicip + $scope.ebs_tags + $scope.ebs_encryption + $scope.ebs_backups + $scope.ec_tags + $scope.ec_nopublicip +$scope.ec_ami + $scope.ec_encryption)/total_count);
                app.speedometerComp = true;
                $scope.classic.needleVal = app.speedometerTarget;
                console.log($scope.rds_tags ,$scope.rds_encryption ,$scope.rds_patching , $scope.rds_backups , $scope.rds_nopublicip , $scope.ebs_tags , $scope.ebs_encryption , $scope.ebs_backups , $scope.ec_tags , $scope.ec_nopublicip ,$scope.ec_ami,$scope.ec_encryption);
                console.log($scope.classic.needleVal);
            }else{
                if (app.speedometerValue == app.speedometerTarget){
                    app.speedometerTarget = Math.floor(Math.random() * 80) + 10
                }else if(app.speedometerTarget>app.speedometerValue){
                    app.speedometerValue++;
                }else{
                    app.speedometerValue--;
                }
                $scope.classic.needleVal = app.speedometerValue;
                if (app.speedometerComp == false || app.speedometerValue!=app.speedometerTarget){
                    $timeout(speedometerVal, 20);
                }

            }
        }
        var getAppElbinfo = function(){
            console.log("fetching app elb information");
            var appName = $scope.currentApp;
            var total_count = 0;
            var tag_count =0;
            var ssl_count = 0;
            var progress = 0;
            $scope.appelb_totalCount = 0;
            $scope.appelb_tags = 0;
            app.appelbprogress =0;
            Overview.getALBs(app.location).then(function(data){              //(appName,app.location)
                total_count = data.data.length;
                $scope.appelb_totalCount = total_count;
                var loadBalancers = data.data;
                for (index in loadBalancers){
                    Overview.getALBinfo(loadBalancers[index]).then(function(lbData){
                        // if (lbData.data.tags.length>0){
                        // 	tag_count++;
                        // 	$scope.appelb_tags = Math.round(tag_count / total_count * 10000)/100 + '%';
                        // }
                        var tempssl = 0;
                        for (sidx in lbData.data.listeners){
                            //port , sslPolicy
                            if (lbData.data.listeners[sidx].port == '443' && lbData.data.listeners[sidx].sslPolicy == null){
                                tempssl++;
                            }
                        }
                        if (tempssl==0){
                            ssl_count++;
                            $scope.appelb_ssl = Math.round(ssl_count / total_count * 10000)/100;
                        }
                        progress++;
                        app.appelbprogress =(progress) / total_count * 100;
                    })
                }
            })
        }


        var getClassicElbinfo = function(){
            console.log("fetching classicloadBalancers");
            var appName = $scope.currentApp;
            var total_count = 0;
            var tag_count =0;
            var progress = 0;
            $scope.elb_totalCount = 0;
            $scope.elb_tags = '0%';
            app.elbprogress =0;
            Overview.getCLBs(appName,app.location).then(function(data){
                total_count = data.data.length;
                $scope.elb_totalCount = total_count;
                var loadBalancers = data.data;
                for (index in loadBalancers){
                    Overview.getCLB(loadBalancers[index]).then(function(lbData){
                        if (lbData.data.tags.length>0){
                            tag_count++;
                            $scope.elb_tags = Math.round(tag_count / total_count * 10000)/100 + '%';
                        }
                        progress++;
                        app.elbprogress =(progress) / total_count;
                    })
                }
            })
        }

        var getASVDetails = function(){
            var appName = 'ASV'+$scope.currentApp;
            var data = {};
            data.name = appName;
            Overview.getDetails(data).then(function(data){
                app.asv_description = data.data.description;
            });
        }

        $scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
            getFilter();
            getInstanceCount();
            getVolums();
            getASVDetails();
            // getBuckets();
            // getAutoScalingGroups();
            // getDatabases();
            //getAppLoadBalancers();
            // getClassicLoadBalancers();
            // getSecurityGroups();

            getEC2info();
            getEbsinfo();
            getRdsinfo();
            getAppElbinfo();
            $timeout(speedometerVal, 50);
            // getClassicElbinfo();

        });
        var getDiffDay = function(creationDate){
            var date1 = new Date(creationDate);
            var date2 = new Date();
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            return diffDays;
        }
    });