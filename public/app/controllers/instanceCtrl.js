angular.module('instanceController', ['instanceServices','groupServices'])

// Controller: User to control the management page and managing of user accounts
.controller('instanceCtrl', function($scope,$filter,$rootScope,User,Instance,Group) {
	var app = this;
	var appName = $scope.currentApp;
	$rootScope.$emit("showASGFilter", true);
	$scope.collapsePanel = false;
	$scope.currentPage = 0;
	$scope.pageSize = 20;
	var getFilter = function(){
		//east checked
		if ($scope.east){
			app.location = '/act.us-east-1';
		}
		//west checked
		if ($scope.west){
			app.location = '/act.us-west-2';
		}
		//all checked
		if ($scope.east && $scope.west){
			app.location = '';	
		}

		if (!$scope.east && !$scope.west){
			app.location = '';
		}
		instances();
	}
	
	var instances = function (){
		console.log("---------------get instances------------------");
		var appName = $scope.currentApp;
		app.currentInstance={};
		app.loading = true;
		console.log("location=",app.location);
		Instance.getInstances(appName,app.location).then(function(data){
			app.instanceId = data.data;
			app.instances = [];
			app.outinstances = [];
			var getDataCompleted = 0;
			console.log("----------instanceIDS---------------");
			console.log(app.instanceId);
			console.log("----------instanceIDS---------------");
			for (index in app.instanceId){
				Instance.getInstance(app.instanceId[index]).then(function(instance_data){
					app.instances.push(instance_data.data);
					var tags = instance_data.data.tags;
					var instanceName = getName(instance_data.data);
					var stackName = getStackName(instance_data.data);
					app.outinstances.push(instance_data.data);	
					instance_data.data.name=instanceName;
					instance_data.data.stackname=stackName;
					var temp_instance = instance_data.data;
					Instance.getImage(temp_instance.imageId).then(function(image_data){
						var creation_date = image_data.data.creationDate;
						var image_version = image_data.data.name;
						var x = image_version.indexOf('Enc-');
						var final = image_version.substr(0,x+4);
						var version = image_version.substr(x+4,image_version.length);
						temp_instance.final = final;
						temp_instance.version = version;
						var diffDay=getDiffDay(creation_date);
						temp_instance.age = diffDay;
                        temp_instance.encryption = app.isEncrypted(image_data.data.tags);
						if ($scope.npprod){
							for (idx in tags){
								if (tags[idx].value.indexOf("ENVNP")>-1){
									if (app.outinstances.indexOf(temp_instance)<0){
										if (getGroupFilter(tags))
										{
											app.outinstances.push(temp_instance);	
										}
									}
								}
							}
						}
						if ($scope.prprod){
							for (idx in tags){
								if (tags[idx].value.indexOf("ENVPR")>-1){
									if (app.outinstances.indexOf(temp_instance)<0){
										if (getGroupFilter(tags))
										{
											app.outinstances.push(temp_instance);	
										}
									}
								}
							}
						}
						if ($scope.prod){
							for (idx in tags){
								if (tags[idx].value.indexOf("ENVPP")>-1){
									if (app.outinstances.indexOf(temp_instance)<0){
										if (getGroupFilter(tags))
										{
											app.outinstances.push(temp_instance);	
										}
									}
								}
							}
						}
						if (!$scope.npprod && !$scope.prprod && !$scope.prod){
							for (idx in tags){
								if (app.outinstances.indexOf(temp_instance)<0){
									if (getGroupFilter(tags))
									{
										app.outinstances.push(temp_instance);	
									}
								}
								
							}
						}
					});
					getDataCompleted++;
					if (getDataCompleted == app.instanceId.length)
					{
						app.loading = false;
						console.log(app.outinstances);
					}
				});
			}
			if (app.instanceId.length==0){
				app.loading = false;
			}
			
		});

	}
    app.isEncrypted = function(data){
        		var isEnc = false;
        		for (index in data){
            			console.log(data[index].key,data[index].value);
            			if (data[index].key=="ASV" && data[index].value=="ASVAWSAMI"){
                				isEnc = true;
                				break;
                			}
            		}
        		return isEnc;
        	}
	$scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
		getFilter();
	});

	$scope.numberOfPages = function(){
		if (app.outinstances){
			return Math.ceil(app.outinstances.length/$scope.pageSize);
		}else{
			return 1;
		}
	}
	var getDiffDay = function(creationDate){
		var date1 = new Date(creationDate);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		return diffDays;
	}
	var getGroupFilter = function(tags){
		if (!app.groupFilters){
			return true;
		}else{
			var result=-1;
			var groupname = '';
			for (index in tags){
				if (tags[index].key=="awsautoscaling:groupname"){
					groupname = tags[index].value;
					break;
				}
			}
			if (app.groupFilters && groupname.length>0){
				result = app.groupFilters.indexOf(groupname);
			}
			if (result>-1){
				return true;
			}
		}
		return false;
	}
	$rootScope.$on("setGroupSelected", function(event,data){
           setASG(data);
    });
	var setASG = function(groupFilters){
		app.groupFilters = groupFilters;
		getFilter();
	}
	
	console.log("show agsFilter~~~~");
	var getName = function(instance){
		for (index in instance.tags){
			if (instance.tags[index].key=='Name'){
				return instance.tags[index].value;
				break;
			}
		}
	}

	var getStackName = function(instance){
		for (index in instance.tags){
			if (instance.tags[index].key=='aws:cloudformation:stack-name'){
				return instance.tags[index].value;
				break;
			}
		}
	}

	var msToTime=function(duration) {
        var milliseconds = parseInt((duration%1000)/100)
            , seconds = parseInt((duration/1000)%60)
            , minutes = parseInt((duration/(1000*60))%60)
            , hours = parseInt((duration/(1000*60*60))%24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds + ".";
    }
	app.setCurrentInstance = function(item){
		var iamRole ='';
		if (item.iamInstanceProfile!=null){
			var idx = item.iamInstanceProfile.arn.indexOf('/')+1;
			iamRole = item.iamInstanceProfile.arn.substring(idx,item.iamInstanceProfile.arn.length);
		}
		app.currentInstance = item;
		app.currentInstance.iamRole = iamRole;
		$scope.collapsePanel = true;

	}
	app.action=function(act){
		console.log(act);
		var temp_role = {
			rolename : app.currentInstance.iamRole
		}
		Instance.setCredentials(temp_role).then(function(data){
			if (data.data.success == true){
				var temp={
					act : act,
					instanceId : app.currentInstance.instanceId
				}
				Instance.action(temp).then(function(data){
					console.log(data);
				});
			}
		})
	}

	$scope.setEditMode = function(){
		$scope.inputMode=false;
		$scope.editMode=!$scope.editMode;
		$scope.createMode = false;
	}
	$scope.setCreateMode = function(){
		$scope.inputMode=false;
		$scope.createMode = !$scope.createMode;
		$scope.key = '';
		$scope.value = '';

		if ($scope.editMode==true && $scope.createMode==true){
			$scope.inputMode=true;
		}else
		{
			$scope.inputMode=false;
		}
	}
 
})
.filter('startFrom',function(){
	return function(input,start){
		start = +start;
		if (input)
		{
			return input.slice(start);
		}else{
			return null;
		}
		
	}
})