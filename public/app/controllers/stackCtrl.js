angular.module('stackController', ['stackServices'])

// Controller: User to control the management page and managing of user accounts
.controller('stackCtrl', function($scope,$rootScope,Stack,$location) {
	var app = this;
	var appName = $scope.currentApp;
	$rootScope.$emit("showASGFilter", false);
	$scope.collapsePanel = false;
	var getFilter = function(){
		//east checked
		if ($scope.east){
			app.location = '/act.us-east-1';
		}
		//west checked
		if ($scope.west){
			app.location = '/act.us-west-2';
		}
		//all checked
		if ($scope.east && $scope.west){
			app.location = '';	
		}

		if (!$scope.east && !$scope.west){
			app.location = '';
		}
		getStacks();
		console.log(app.location);
	}
	var getStacks = function(){
		app.stackNames = [];
		app.outstacks = [];
		app.tags=[];
		app.volume_data = []; //for snapshot
		var appName = $scope.currentApp;
		Stack.getInstances(appName,app.location).then(function(data){
			var temp = data.data;
			for (index in temp){
				Stack.getInstance(temp[index]).then(function(instance_data){
					var tempTag = instance_data.data.tags;
					for (var i=0; i<tempTag.length; i++){
						if (tempTag[i].key=="aws:cloudformation:stack-name"){//aws:cloudformation:stack-name
							//--get for snapshot
							for (volidx in instance_data.data.blockDeviceMappings){
								if (instance_data.data.rootDeviceName==instance_data.data.blockDeviceMappings[volidx].deviceName) continue;
								var tempVol = {};
								tempVol.deviceName = instance_data.data.blockDeviceMappings[volidx].deviceName;
								tempVol.volumeId = instance_data.data.blockDeviceMappings[volidx].ebs.volumeId;
								tempVol.stackName = tempTag[i].value;
								tempVol.instanceId = temp[index];
								app.volume_data.push(tempVol);
							}
							//--get for snapshot
							if (tempTag[i].value=='Stack2') continue;//for bug fix
							if (app.stackNames.indexOf(tempTag[i].value)==-1){
								app.stackNames.push(tempTag[i].value);
								Stack.getStack(tempTag[i].value).then(function(stack_data){
									var tempStackData=stack_data.data;
									tempStackData.age = getDiffDay(tempStackData.creationTime);
									tempStackData.tags = app.tags[index];
									app.outstacks.push(tempStackData);
								});
							}
							break;
						}
					}
				});
			}
		});
	}


	$scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
		getFilter();
	});

	//for snapshot
	$scope.rehydrate = function(stackName){
		console.log(stackName);
		var latest_snapshot = '';
		var creationTime = '';
		var state = '';
		var snapshots = [];
		for (index in app.volume_data){
			if (app.volume_data[index].stackName==stackName){
				console.log("get snapshot");
				Stack.getSnapshots(app.volume_data[index]).then(function(data){
					console.log("get latest snapshot");
					for (snapidx in data.data){
						if (snapshots.indexOf(data.data[snapidx])==-1){
							snapshots.push(data.data[snapidx]);
						}
						if (index == app.volume_data.length-1){
							for (vindex in snapshots){
								Stack.getSnapshot(snapshots[vindex]).then(function(data){
									if (latest_snapshot == ''){
										latest_snapshot = data.data.snapshotId;
										creationTime = data.data.startTime;
										state = data.data.state;
									}else{
										if (app.compareTime(creationTime,data.data.startTime)){
											latest_snapshot = data.data.snapshotId;
											creationTime = data.data.startTime;
											state = data.data.state;
										}
									}
									if (vindex==snapshots.length-1){
										console.log('/stacklist/rehydration/'+stackName +'/'+latest_snapshot+'/'+creationTime+'/'+state);
										$location.path('/stacklist/rehydration/'+stackName +'/'+latest_snapshot+'/'+creationTime+'/'+state);
									}
								});
							}
						}
					}
				});
			}
		}
	}
	app.compareTime = function(time1,time2){
		if (Date.parse(time1) < Date.parse(time2)){
			return true;
		}else{
			return false;
		}
	}
	//for snapshot

	app.setCurrentStack = function(item){
		app.currentStack=item;
		app.currentResources = [];
		for (index in item.resources){
			for (idx in item.resources[index]){
				app.currentResources.push(item);
			}
		}
		// console.log(app.currentStack);
		// console.log(app.currentResources);
		$scope.collapsePanel = false;
	}
	var getDiffDay = function(creationDate){
		var date1 = new Date(creationDate);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		return diffDays;
	}
 
});
