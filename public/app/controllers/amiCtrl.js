angular.module('amiController', ['amiServices'])

    // Controller: User to control the management page and managing of user accounts
    .controller('amiCtrl', function($scope,$filter,$rootScope,User,Ami) {
        var app = this;
        var appName = $scope.currentApp;
        //$rootScope.$emit("showASGFilter", true);
        $scope.collapsePanel = false;
        $scope.currentPage = 0;
        $scope.pageSize = 16;
        var getFilter = function () {
            //east checked
            if ($scope.east) {
                app.location = '/act.us-east-1';
            }
            //west checked
            if ($scope.west) {
                app.location = '/act.us-west-2';
            }
            //all checked
            if ($scope.east && $scope.west) {
                app.location = '';
            }

            if (!$scope.east && !$scope.west) {
                app.location = '';
            }
            amiIds();
        }

        var amiIds = function () {
            console.log("---------------get amiIds------------------");
                var appName = $scope.currentApp;
            app.currentamiId = {};
            app.loading = true;
            console.log("location=", app.location);
            Ami.getAmiIds(appName,app.location).then(function (data) {
                app.amiId = data.data;
                app.amiIds = [];
                app.outamiIds = [];
                var getDataCompleted = 0;
                console.log("----------amiIDS---------------");
                console.log(app.amiId);
                console.log("----------amiIDS---------------");
                for (index in app.amiId) {
                    Ami.getAmiId(app.amiId[index]).then(function (ami_data) {
                        app.outamiIds.push(ami_data.data);
                        getDataCompleted++;
                        if (getDataCompleted == app.amiId.length) {
                            app.loading = false;
                            console.log(app.outamiIds);
                        }
                    });
                }
            });
        }
        $scope.$watchGroup(['east', 'west'], function (data) {
            getFilter();
        });
        $scope.numberOfPages = function () {
            if (app.outamiIds) {
                return Math.ceil(app.outamiIds.length / $scope.pageSize);
            } else {
                return 1;
            }
        }
        app.setCurrentamiID = function (data){
            app.currentAmiId = data;
        }
        var getName = function (instance) {
            for (index in instance.tags) {
                if (instance.tags[index].key == 'Name') {
                    return instance.tags[index].value;
                    break;
                }
            }
        }
    })