angular.module('loginController',['authServices'])
    .controller('ssotokenCtrl',function($routeParams,$location,$window,Auth){
        var code = $location.absUrl().substr($location.absUrl().indexOf('code=')+5,$location.absUrl().length);
        Auth.getAuthtoken(code).then(function(data){
            console.log(data.data);
            Auth.getUserInfo(data.data.access_token).then(function(user_data){
                console.log(user_data.data);
                $location.path('/overview');
            });
        });
    })
    .controller('loginCtrl',function($window,$location,Auth){
        if (Auth.isLoggedIn()){
            $location.path('/overview');
        }else{
            $window.location.href='https://webaccessqa.kdc.capitalone.com/as/authorization.oauth2?client_id=ThickClientOAuthwPingSSO4AXON&redirect_uri=http://axon-poc.clouddqt.capitalone.com/getAuth&scope=openid profile mobileapp&response_type=code';
        }
    });