angular.module('ebsVolumeController', ['ebsVolumeServices'])

// Controller: User to control the management page and managing of user accounts
.controller('ebsVolumeCtrl', function($scope,$rootScope,Volume) {
	var app = this;
	var appName = $scope.currentApp;
	$rootScope.$emit("showASGFilter", false);
	$scope.collapsePanel = false;
	$scope.currentPage = 0;
	$scope.pageSize = 20;
	var getFilter = function(){
		//east checked
		if ($scope.east){
			app.location = '/act.us-east-1';
		}
		//west checked
		if ($scope.west){
			app.location = '/act.us-west-2';
		}
		//all checked
		if ($scope.east && $scope.west){
			app.location = '';	
		}

		if (!$scope.east && !$scope.west){
			app.location = '';
		}
		volume();
	}
	var volume = function(){
		console.log("---------------get volume--------------");
		var appName = $scope.currentApp;
		app.currentVolume={};
		app.loading = true;

		Volume.getVolumes(appName,app.location).then(function(data){
			app.volumeId = data.data;
			app.volumes = [];
			app.outvolumes = [];
			var getDataCompleted = 0;
			console.log(data.data);
			for (index in app.volumeId){
				Volume.getVolume(app.volumeId[index]).then(function(volume_data){
					app.volumes.push(volume_data.data);
					console.log(volume_data.data);
					var tags = volume_data.data.tags;
					var volumeName = getName(volume_data.data);
					volume_data.data.name=volumeName;
					if ($scope.npprod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVNP")>-1){
								app.outvolumes.push(volume_data.data);
							}
						}
					}
					if ($scope.prprod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVPR")>-1){
								app.outvolumes.push(volume_data.data);
							}
						}
					}
					if ($scope.prod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVPP")>-1){
								app.outvolumes.push(volume_data.data);
							}
						}
					}
					if (!$scope.npprod && !$scope.prprod && !$scope.prod){
						console.log("-------------------------");
						console.log(volume_data.data);
						console.log("-------------------------");
						for (idx in tags){
							if (app.outvolumes.indexOf(volume_data.data)<0)
							{
								app.outvolumes.push(volume_data.data);
							}
						}
						console.log("result",app.outvolumes);
					}
					getDataCompleted++;
					console.log(getDataCompleted,app.volumeId.length);
					if (getDataCompleted == app.volumeId.length)
					{
						app.loading = false;
					}
					
				});
			}
			if (app.volumeId.length==0){
				app.loading = false;
			}
		});

	}
	var getName = function(volume){
		for (index in volume.tags){
			if (volume.tags[index].key=='Name'){
				return volume.tags[index].value;
				break;
			}
		}
	}
	$scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
		getFilter();
	});
	app.setCurrentVolume = function(item){
		app.description='';

		for (index in item.attachments){
			app.description+="instanceId : "+item.attachments[index].instanceId+'\n';
			app.description+="state : "+item.attachments[index].state+'\n';
			app.description+="attachTime : " + item.attachments[index].attachTime+'\n';
			app.description+="deleteOnTermination : " + item.attachments[index].deleteOnTermination+'\n';
			app.description+="device : "+item.attachments[index].device+'\n\n\n';
		}
		app.description=app.description.trim();
		app.currentVolume = item;
		$scope.collapsePanel = true;
	}
	$scope.setEditMode = function(){
		$scope.inputMode=false;
		$scope.editMode=!$scope.editMode;
		$scope.createMode = false;
	}
	$scope.setCreateMode = function(){
		$scope.inputMode=false;
		$scope.createMode = !$scope.createMode;
		$scope.key = '';
		$scope.value = '';

		if ($scope.editMode==true && $scope.createMode==true){
			$scope.inputMode=true;
		}else
		{
			$scope.inputMode=false;
		}
	}
	$scope.numberOfPages = function(){
		if (app.outvolumes){
			return Math.ceil(app.outvolumes.length/$scope.pageSize);
		}else{
			return 1;
		}
		
	}

})
.filter('startFrom',function(){
	return function(input,start){
		start = +start;
		if (input)
		{
			return input.slice(start);
		}else{
			return null;
		}
		
	}
})