angular.module('mainController', ['authServices', 'userServices','groupServices'])

    .controller('mainCtrl', function(Auth,Group, $timeout, $location, $rootScope, $window, $interval, User, AuthToken ,$scope) {
        var app = this;
        app.loadme = false; // Hide main HTML until data is obtained in AngularJS
        app.navshow=false;
        $scope.east=true;
        app.selectedASG=[];
        app.asgShow = false;
        app.fetchData = false;
        console.log("hide~~~~~~~~~~~~~~~~~");
        //cheking session per 60000ms = 1min

        // Function to run an interval that checks if the user's token has expired
        app.checkSession = function() {
            if (app.fetchData==false && Auth.isLoggedIn()){
                console.log("fetchData==false,Auth_login==true");
                $scope.name = Auth.getName();
                app.getAppName();
                app.checkingSession = true; // Use variable to keep track if the interval is already running
                app.navshow = true;
                app.fetchData = true;
            }else if(app.fetchData==true && !Auth.isLoggedIn()){
                console.log("fetchData==true,Auth_login==false");
                app.fetchData = false;
                app.navshow = false;
                $interval.cancel(stopTime);
                app.logout();
            }else{
                console.log(app.fetchData,Auth.isLoggedIn())
                console.log("there is no action.")
            }
        };
        var stopTime = $interval(app.checkSession(), 3600000);
        //app.checkSession(); // Ensure check is ran check, even if user refreshes

        app.getAppName = function(){
            var appNames=[];
            var temp={};
            temp.eid=Auth.getUserId();
            console.log("MainCtrl getAppname function, userid is ",Auth.getUserId());
            Auth.getAppname(temp).then(function(appnames){
                console.log("~~~~~~~~asvname~~~~~~")
                console.log(appnames);
                console.log("~~~~~~~~end asvname~~~~~~")
                var data=appnames;
                if (data){
                    for (index in data){
                        var temp = data[index];
                        if (temp){
                            var asvname = temp.substr(0,3);
                            if (asvname=='ASV'){
                                asvname = temp.substr(3,temp.length);
                            }else{
                                asvname = temp.substr(0,temp.length);
                            }
                            console.log(asvname);
                            if (asvname) {
                                if (appNames.indexOf(asvname)==-1)
                                {
                                    if (index==0) {
                                        if (!$scope.currentApp)
                                        {
                                            $scope.currentApp = asvname;
                                            //     var appName = $scope.currentApp;
                                            //     Group.getGroups(appName,"").then(function(data){
                                            //         app.groups = data.data;
                                            //     });
                                        }
                                    }
                                    appNames.push(asvname);
                                }
                            }
                        }
                    }
                }
            });
            app.appNames = appNames;
        }



        // Will run code every time a route changes
        $rootScope.$on('$routeChangeStart', function() {
            if (!app.checkingSession) app.checkSession();
            // Check if user is logged in
            if (Auth.isLoggedIn()) {
                app.isLoggedIn = true; // Variable to activate ng-show on index
                app.getAppName();
                // app.getGroupNames();
            } else {
                app.isLoggedIn = false; // User is not logged in, set variable to falses
                app.loadme = true; // Show main HTML now that data is obtained in AngularJS
            }
            if ($location.hash() == '_=_') $location.hash(null); // Check if facebook hash is added to URL
            app.disabled = false; // Re-enable any forms
            app.errorMsg = false; // Clear any error messages

        });


        // get current app when user click the group in the panel
        app.setCurrentApp = function(item){
            $scope.currentApp = item;
        };
        // Function to logout the user
        app.logout = function() {
            Auth.logout();
            $location.path('/');
            app.navshow=false;
        };
        app.setASG = function(item){
            if (app.selectedASG.indexOf(item)<0){
                app.selectedASG.push(item)
            }else{
                var index = app.selectedASG.indexOf(item);
                app.selectedASG.splice(index,1);
            }
            console.log(app.selectedASG);
            $rootScope.$emit("setGroupSelected", app.selectedASG);
        }

        $scope.$watch('currentApp',function(appName){
            app.getGroupsFilter(appName);
        });
        app.getGroupsFilter = function(appName){
            app.groups='';
            Group.getGroups(appName,"").then(function(data){
                app.groups = data.data;
            });
        }
        $rootScope.$on("showASGFilter", function(event,data){
            app.asgShow = data;
        });
        // app.getGroupNames = function(){
        //     console.log("-------get groups----------");
        //     var appName = $scope.currentApp;
        //     Group.getGroups(appName,"").then(function(data){
        //         app.groups = data.data;
        //         console.log("groups",app.groups);
        //     });
        //     console.log("-----------get groups end-----------");
        // }
    });