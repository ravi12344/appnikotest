angular.module('snapshotController',[])

// Controller: User to control the management page and managing of user accounts
.controller('snapshotCtrl', function($scope,$rootScope,Snapshot) {
	var app = this;
	var appName = $scope.currentApp;
	$rootScope.$emit("showASGFilter", false);
	$scope.collapsePanel = false;
	$scope.currentPage = 0;
	$scope.pageSize = 10;
	var getFilter = function(){
		//east checked
		if ($scope.east){
			app.location = '/act.us-east-1';
		}
		//west checked
		if ($scope.west){
			app.location = '/act.us-west-2';
		}
		//all checked
		if ($scope.east && $scope.west){
			app.location = '';	
		}

		if (!$scope.east && !$scope.west){
			app.location = '';
		}
		snapshots();
	}
	var snapshots = function (){
		console.log("---------------get snapshots------------------");
		var appName = $scope.currentApp;
		app.currentSnapshot={};
		app.loading = true;
		Snapshot.getSnapshots(appName,app.location).then(function(data){
			app.snapshotId = data.data;
			app.snapshots = [];
			app.outsnapshots = [];
			var getDataCompleted = 0;
			for (index in app.snapshotId){
				Snapshot.getSnapshot(app.snapshotId[index]).then(function(snapshot_data){
					app.snapshots.push(snapshot_data.data);
					var tags = snapshot_data.data.tags;
					if ($scope.npprod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVNP")>-1){
								if (app.outsnapshots.indexOf(snapshot_data.data)<0){
									app.outsnapshots.push(snapshot_data.data);	
								}
							}

						}
					}
					if ($scope.prprod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVPR")>-1){
								if (app.outsnapshots.indexOf(snapshot_data.data)<0){
									app.outsnapshots.push(snapshot_data.data);	
								}
							}
						}
					}
					if ($scope.prod){
						for (idx in tags){
							if (tags[idx].value.indexOf("ENVPP")>-1){
								if (app.outsnapshots.indexOf(snapshot_data.data)<0){
									app.outsnapshots.push(snapshot_data.data);	
								}
							}
						}
					}
					if (!$scope.npprod && !$scope.prprod && !$scope.prod){
						for (idx in tags){
							if (app.outsnapshots.indexOf(snapshot_data.data)<0){
									app.outsnapshots.push(snapshot_data.data);	
							}
						}
					}
					getDataCompleted++;
					if (getDataCompleted == app.snapshotId.length)
					{
						app.loading = false;
					}
				});
			}
			if (app.snapshotId.length==0){
				app.loading=false;
			}
		});

	}
	$scope.$watchGroup(['currentApp','east','west','npprod','prprod','prod'],function(data){
		getFilter();
	});
	app.setCurrentSnapshot = function(item){
		app.description='';
		for (index in item.attachments){
			app.description+="snapshotId : "+item.attachments[index].instanceId+'\n';
			app.description+="state : "+item.attachments[index].state+'\n';
			app.description+="attachTime : " + item.attachments[index].attachTime+'\n';
			app.description+="deleteOnTermination : " + item.attachments[index].deleteOnTermination+'\n';
			app.description+="device : "+item.attachments[index].device+'\n\n\n';
		}
		app.currentSnapshot = item;
		$scope.collapsePanel = true;
	}
	$scope.setEditMode = function(){
		$scope.inputMode=false;
		$scope.editMode=!$scope.editMode;
		$scope.createMode = false;
	}
	$scope.setCreateMode = function(){
		$scope.inputMode=false;
		$scope.createMode = !$scope.createMode;
		$scope.key = '';
		$scope.value = '';

		if ($scope.editMode==true && $scope.createMode==true){
			$scope.inputMode=true;
		}else
		{
			$scope.inputMode=false;
		}
	}
	$scope.numberOfPages = function(){
		if (app.outsnapshots){
			return Math.ceil(app.outsnapshots.length/$scope.pageSize);
		}else{
			return 1;
		}
		
	}
})
.filter('startFrom',function(){
	return function(input,start){
		start = +start;
		if (input)
		{
			return input.slice(start);
		}else{
			return null;
		}
		
	}
})