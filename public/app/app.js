angular.module('userApp', ['appRoutes','mainController','loginController','groupController','groupServices','userControllers','configServices','userServices','authServices','eddaServices','instanceController','overviewController','overviewServices','stackServices','stackController','amiController','amiServices','snapshotController','instanceServices','ebsVolumeServices','ebsVolumeController','snapshotServices','rehydrationController','rehydrationServices'])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

    }
    ]);
//.config(['$httpProvider', function($httpProvider) {
//        $httpProvider.defaults.useXDomain = true;
//        delete $httpProvider.defaults.headers.common['X-Requested-With'];
//    }
//]);



