var app = angular.module('appRoutes', ['ngRoute'])

    // Configure Routes; 'authenticated = true' means the user must be logged in to access the route
    .config(function($routeProvider, $locationProvider) {

        // AngularJS Route Handler
        $routeProvider


        // Route: Home

            .when('/getAuth', {
                templateUrl: 'app/views/pages/home.html',
                controller: 'ssotokenCtrl',
                controllerAs:'sso',
                authenticated:false
            })

            .when('/ami',{
                templateUrl: 'app/views/pages/ami_list.html',
                controller: 'amiCtrl',
                controllerAs:'ami',
                authenticated: true
            })
            .when('/', {
                templateUrl: 'app/views/pages/home.html',
                controller:'loginCtrl',
                controllerAs:'login',
                authenticated: false,
            })
            .when('/home',{
                templateUrl: 'app/views/pages/index.html',
                authenticated: true
            })
            .when('/register', {
                templateUrl: 'app/views/pages/register.html',
                controller: 'regCtrl',
                controllerAs: 'register',
                authenticated: false
            })
            .when('/overview', {
                templateUrl: 'app/views/pages/overview.html',
                controller : 'overviewCtrl',
                controllerAs : 'overview',
                authenticated: true
            })
            .when('/create', {
                templateUrl: 'app/views/pages/createtreeview.html',
                controller: 'createTreeCtrl',
                controllerAs: 'createTree',
                authenticated: true
            })
            .when('/instancelist',{
                templateUrl: 'app/views/pages/instance_list.html',
                controller: 'instanceCtrl',
                controllerAs:'instance',
                authenticated: true
            })
            .when('/instancetype',{
                templateUrl: 'app/views/pages/instance_type.html',
                controller: 'instanceCtrl',
                controllerAs:'instance',
                authenticated: true
            })
            .when('/snapshot',{
                templateUrl: 'app/views/pages/snapshot.html',
                controller: 'snapshotCtrl',
                controllerAs:'snapshot',
                authenticated: true
            })
            .when('/groups',{
                templateUrl: 'app/views/pages/groups.html',
                controller: 'groupCtrl',
                controllerAs:'group',
                authenticated: true
            })
            .when('/stacklist',{
                templateUrl: 'app/views/pages/stacks_list.html',
                controller: 'stackCtrl',
                controllerAs:'stack',
                authenticated: true
            })
            .when('/stacklist/rehydration/:stackName/:latest_snapshot/:creationTime/:state',{
                templateUrl: 'app/views/pages/rehydration.html',
                controller: 'rehydrationCtrl',
                controllerAs:'rehydration',
                authenticated:true
            })
            .when('/ebsvolume',{
                templateUrl: 'app/views/pages/ebs_volume.html',
                controller: 'ebsVolumeCtrl',
                controllerAs:'ebsVolume',
                authenticated: true
            })

            .when('/snapshot',{
                templateUrl: 'app/views/pages/snapshot.html',
                controller: 'snapshotCtrl',
                controllerAs:'snapshot',
                authenticated: true
            })

            // Route: About Us (for testing purposes)
            .when('/about', {
                templateUrl: 'app/views/pages/about.html'
            })

            .otherwise({ redirectTo: '/' }); // If user tries to access any other route, redirect to home page

        $locationProvider.html5Mode({ enabled: true, requireBase: false }); // Required to remove AngularJS hash from URL (no base is required in index file)
    });

// Run a check on each route to see if user is logged in or not (depending on if it is specified in the individual route)
app.run(['$rootScope', 'Auth', '$location', 'User', function($rootScope, Auth, $location,$window, User) {

    // Check each time route changes
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        // Only perform if user visited a route listed above
        if (next.$$route !== undefined) {
            // Check if authentication is required on route
            if (next.$$route.authenticated === true) {
                console.log("routes----------------", Auth.isLoggedIn());
                // Check if authentication is required, then if permission is required
                if (!Auth.isLoggedIn()) {
                    event.preventDefault(); // If not logged in, prevent accessing route
                    //$location.path('/'); // Redirect to home instead
                    $location.url(' path https://webaccessqa.kdc.capitalone.com/as/authorization.oauth2?client_id=ThickClientOAuthwPingSSO4AXON&redirect_uri=http://axon-poc.clouddqt.capitalone.com/getAuth&scope=openid profile mobileapp&response_type=code');
                } else {
                    //$location.path('/overview'); // Redirect to profile instead
                }
            } else if (next.$$route.authenticated === false) {
                // If authentication is not required, make sure is not logged in
                if (Auth.isLoggedIn()) {
                    // event.preventDefault(); // If user is logged in, prevent accessing route
                    $location.path('/overview'); // Redirect to profile instead
                }
            }
        }
    });
}]);